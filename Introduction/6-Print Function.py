def process(x):
    for i in range(1, x + 1):
        print(i, end='')


if __name__ == '__main__':
    n = int(input())

    process(n)
