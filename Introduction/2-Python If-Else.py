def process(x):
    if x % 2 != 0:
        return 'Weird'
    elif x in range(2, 6):
        return 'Not Weird'
    elif x in range(6, 21):
        return 'Weird'
    else:
        return 'Not Weird'


if __name__ == '__main__':
    n = int(input())

    print(process(n))
