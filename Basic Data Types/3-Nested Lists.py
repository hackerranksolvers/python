def second_lowest(a):
    temp = sorted(a, key=lambda y: (y[1]), reverse=True)

    max_score = temp[-1][1]
    while temp[-1][1] == max_score:
        temp.pop()

    max_score = temp[-1][1]
    result = list()
    for element in temp:
        if element[1] == max_score:
            result.append(element[0])

    for item in sorted(result):
        print(item)


if __name__ == '__main__':
    students = list()

    n = int(input())

    for i in range(n):
        name = str(input())
        score = float(input())
        students.append([name, score])

    second_lowest(students)
