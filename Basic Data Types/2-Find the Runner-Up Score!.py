def runner_up(x, k):
    temp = sorted(set(x))
    return temp[len(temp) - 2]


if __name__ == '__main__':
    n = int(input())
    a = list(map(int, input().split()))

    print(runner_up(a, n))
