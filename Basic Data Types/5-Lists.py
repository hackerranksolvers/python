def process(x, command):
    if command[0] == 'insert':
        x.insert(int(command[1]), int(command[2]))
    elif command[0] == 'print':
        print(x)
    elif command[0] == 'remove':
        x.remove(int(command[1]))
    elif command[0] == 'append':
        x.append(int(command[1]))
    elif command[0] == 'sort':
        x.sort()
    elif command[0] == 'pop':
        x.pop()
    else:
        x.reverse()


if __name__ == '__main__':
    a = list()

    n = int(input())

    for i in range(n):
        line = input().split()
        process(a, line)
