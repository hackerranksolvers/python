def process(a, x):
    return sum(a[x]) / 3


if __name__ == '__main__':
    records = dict()

    n = int(input())

    for i in range(n):
        name, *line = input().split()
        score = list(map(float, line))

        records[name] = score

    query = input()

    print('{:.2f}'.format(process(records, query)))
