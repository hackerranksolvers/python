def hashed(x):
    return hash(x)


if __name__ == '__main__':
    n = int(input())
    a = tuple(map(int, input().split()))

    print(hashed(a))
