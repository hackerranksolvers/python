def process(l, b, h, x):
    return [[i, j, k] for i in range(l + 1) for j in range(b + 1) for k in range(h + 1) if (i + j + k) != x]


if __name__ == '__main__':
    x = int(input())
    y = int(input())
    z = int(input())
    n = int(input())

    print(process(x, y, z, n))
